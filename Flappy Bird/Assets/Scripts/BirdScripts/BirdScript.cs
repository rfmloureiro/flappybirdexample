﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdScript : MonoBehaviour {

    public static BirdScript instance;

    [SerializeField]
    private Rigidbody2D myRigidBody;

    [SerializeField]
    private Animator anim;

    private float fowardspeed = 3f;

    private float bounceSpeed = 4f;

    private bool didFlap = false;

    public bool isAlive;

    private Button flapButton;
    
    private void Awake() {
        if(instance == null) {
            instance = this;
        }
        isAlive = true;
        flapButton = GameObject.FindGameObjectWithTag("FlapButton").GetComponent<Button>();
        flapButton.onClick.AddListener( () => flapTheBird());
        SetCameraX();
    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void FixedUpdate() {
        if (isAlive) {
            
            Vector3 temp = transform.position;
            temp.x += fowardspeed * Time.deltaTime;
            transform.position = temp;
       

            if (didFlap) {
                didFlap = false;
                myRigidBody.velocity = new Vector2(0, bounceSpeed);
                anim.SetTrigger("Flap");
            }

           if(myRigidBody.velocity.y >= 0) {

                //transform.rotation = Quaternion.Euler(0, 0, 0);
                float angle = 0;
                angle = Mathf.Lerp(0, 90, myRigidBody.velocity.y / 10);
                transform.rotation = Quaternion.Euler(0, 0, angle);
            } else {
                float angle = 0;
                angle = Mathf.Lerp(0, -90, -myRigidBody.velocity.y / 10);
                transform.rotation = Quaternion.Euler(0, 0, angle);
            }

        }
    }


    public void SetCameraX() {
        CameraScript.offsetX = (Camera.main.transform.position.x - transform.position.x) - 1f;
    }
    public void flapTheBird() {
        didFlap = true;
    }

    public float GetPositionX() {
        return transform.position.x;
    }
}
